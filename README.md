# build status

main branch: [![main branch status](https://gitlab.com/adjectivenoun/appiware/badges/main/pipeline.svg "main branch status")](https://gitlab.com/adjectivenoun/appiware/-/pipelines/911172376)

build_ci branch: [![build_ci branch status](https://gitlab.com/adjectivenoun/appiware/badges/build_ci/pipeline.svg "build_ci branch status")](https://gitlab.com/adjectivenoun/appiware/-/pipelines/911172376)


# build
```
rustup target add wasm32-unknown-unknown

cargo build --release --bin echo_server_http_axum --target wasm32-unknown-unknown
```
