load("@bazel_tools//tools/build_defs/repo:http.bzl", "http_archive")

# Get the rules_rust bazel rules, for things like rust_library and rust_binary.
http_archive(
    name = "rules_rust",
    sha256 = "4a9cb4fda6ccd5b5ec393b2e944822a62e050c7c06f1ea41607f14c4fdec57a2",
    urls = ["https://github.com/bazelbuild/rules_rust/releases/download/0.25.1/rules_rust-v0.25.1.tar.gz"],
)

# Set up Starlark library.
# Required by: rules_rust, maybe proto rules
http_archive(
    name = "bazel_skylib",
    sha256 = "4756ab3ec46d94d99e5ed685d2d24aece484015e45af303eb3a11cab3cdc2e71",
    strip_prefix = "bazel-skylib-1.3.0",
    urls = ["https://github.com/bazelbuild/bazel-skylib/archive/refs/tags/1.3.0.zip"],
)

load("@bazel_skylib//:workspace.bzl", "bazel_skylib_workspace")

bazel_skylib_workspace()

load("@rules_rust//rust:repositories.bzl", "rules_rust_dependencies", "rust_register_toolchains")
load("@rules_rust//tools/rust_analyzer:deps.bzl", "rust_analyzer_dependencies")

rules_rust_dependencies()

rust_analyzer_dependencies()

rust_register_toolchains(
    edition = "2021",
    versions = [
        "1.70.0",
    ],
)

# Load our list of cargo manifests and generate BUILD files for all external
# crates in them.
load("//:bazel/external_crates.bzl", "external_crates_repository")

external_crates_repository(
    name = "crate_index",
)

load("@crate_index//:defs.bzl", "crate_repositories")

crate_repositories()
