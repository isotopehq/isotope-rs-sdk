load("@rules_rust//crate_universe:defs.bzl", "crates_repository", "crate", "splicing_config")

# This reads all the crates in the cargo.toml files, downloads them into an
# external repository with the given name, and generates BUILD files for them.
def external_crates_repository(name):
    crates_repository(
        name = name,
        cargo_lockfile = "//:Cargo.lock",
        lockfile = "//:Cargo.Bazel.lock",
        manifests = [
            "//:Cargo.toml",
        ],
    )

