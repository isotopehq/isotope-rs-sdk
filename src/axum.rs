use std::collections::BTreeMap;

use log::{debug, info};
use serde_json::value::Value as JsonValue;

use crate::host;
use crate::struct_fs;

use struct_fs::path;
use struct_fs::protocols::http::{Request as HttpRequest, Response as HttpResponse};
use struct_fs::protocols::server::*;

const OUTSTANDING_REQUESTS_PREFIX: &str = "outstanding";

type RequestId = u64;

fn as_axum_request(
    http_request: HttpRequest,
) -> anyhow::Result<axum::http::Request<axum::body::Body>> {
    use std::str::FromStr;

    let method = axum::http::Method::from_str(&http_request.method)?;
    let uri = axum::http::Uri::from_str(&http_request.uri)?;
    let body = http_request.body;

    let mut builder = axum::http::Request::builder().method(method).uri(uri);

    for (k, v) in http_request.headers.iter() {
        builder = builder.header(k, v);
    }

    Ok(builder.body(axum::body::Body::from(body))?)
}

pub async fn from_axum_request(
    mut axum_request: axum::http::Request<axum::body::Body>,
) -> HttpRequest {
    let body_bytes = match axum::body::HttpBody::data(axum_request.body_mut()).await {
        None => vec![],
        Some(body_bytes) => match body_bytes {
            Ok(bytes) => bytes.to_vec(),
            Err(_) => vec![],
        },
    };

    HttpRequest {
        uri: axum_request.uri().to_string(),
        method: axum_request.method().to_string(),
        headers: axum_request.headers().clone(),
        body: body_bytes,
    }
}

// TODO(alex): Figure out how to make this into Tower Service middleware.
pub async fn wrap_for_server_protocol(mut router: axum::Router) -> anyhow::Result<()> {
    use tower_service::Service;
    info!("Running Axum server...");

    let requests_path = path!(SERVER_REQUESTS_PATH);
    let outstanding_requests_path = requests_path.join(&path!(OUTSTANDING_REQUESTS_PREFIX));

    let mut responses: BTreeMap<RequestId, axum::http::Response<axum::body::BoxBody>> =
        BTreeMap::new();
    let mut next_request_id: RequestId = 0;

    while let Some(mut request) = host::store::read::<Request>(&requests_path)? {
        debug!("Handling request: {:?}", request);
        match request.kind {
            RequestType::Read => {
                let request_id: RequestId = match request.path[request.path.len() - 1].parse() {
                    Ok(id) => id,
                    Err(error) => {
                        host::store::write(
                            &request.request,
                            &Response::<()>::Read(Err(format!(
                                "Error encountered on Read request for path {}: {}",
                                request.path, error
                            ))),
                        )?;
                        continue;
                    }
                };

                if let Some(mut axum_response) = responses.remove(&request_id) {
                    let body_bytes =
                        match axum::body::HttpBody::data(axum_response.body_mut()).await {
                            None => vec![],
                            Some(body_bytes) => match body_bytes {
                                Ok(bytes) => bytes.to_vec(),
                                Err(_) => vec![],
                            },
                        };

                    let response = HttpResponse {
                        status: axum_response.status().as_u16(),
                        headers: axum_response.headers().clone(),
                        body: body_bytes,
                    };
                    host::store::write(
                        &request.request,
                        Response::<&HttpResponse>::Read(Ok(Some(&response))),
                    )?;
                    debug!("Request fulfilled with write/{} ...", request.request);
                    continue;
                } else {
                    host::store::write(
                        &request.request,
                        &Response::<()>::Read(Err(format!(
                            "No matching server protocol write request found for read: {:?}",
                            request
                        ))),
                    )?;
                }
            }
            RequestType::Write => {
                let request_id = next_request_id;
                next_request_id += 1;

                debug!("Building Axum request");
                let http_request: HttpRequest =
                    match serde_json::from_value(request.data.take().unwrap_or(JsonValue::Null)) {
                        Ok(req) => req,
                        Err(error) => {
                            host::store::write(
                                &request.request,
                                &Response::<()>::Write(Err(format!(
                                    concat!(
                                        "An error occurred when fulfilling Write request ",
                                        "on path {}: {}"
                                    ),
                                    request.path, error
                                ))),
                            )?;
                            continue;
                        }
                    };

                let axum_request = match as_axum_request(http_request) {
                    Ok(req) => req,
                    Err(error) => {
                        host::store::write(
                            &request.request,
                            &Response::<()>::Write(Err(format!(
                                concat!(
                                    "An error occurred when converting from ",
                                    "StructFS::http:Request to axum::http::Request ",
                                    "(write on path {}): {}"
                                ),
                                request.path, error
                            ))),
                        )?;
                        continue;
                    }
                };

                debug!("Calling into axum router w/ request: {axum_request:?}");
                let axum_response = router.call(axum_request).await.unwrap();
                responses.insert(request_id, axum_response);

                let response_path =
                    outstanding_requests_path.join(&path!(&format!("{request_id}")));

                host::store::write(
                    &request.request,
                    &Response::<()>::Write(Ok(response_path.clone())),
                )?;
                debug!(
                    "Request fulfilled with `write/{} {}`",
                    &request.request, response_path
                );
            }
        }
    }
    info!("Synchronous request read returned None");

    info!("Axum shutting down...");
    Ok(())
}
