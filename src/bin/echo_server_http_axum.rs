#![no_main]
use log::{debug, info};

use appiware_sdk::host;
use appiware_sdk::struct_fs;
use struct_fs::protocols::http::Request as HttpRequest;

pub fn error_to_500(error: struct_fs::StoreError) -> (axum::http::StatusCode, axum::Json<String>) {
    (
        axum::http::StatusCode::INTERNAL_SERVER_ERROR,
        axum::Json(format!("Internal server error: {}", error)),
    )
}

fn build_router() -> axum::Router {
    axum::Router::new()
        .route("/", axum::routing::any(echo))
        .route("/*path", axum::routing::any(echo))
}

async fn echo(
    request: axum::http::Request<axum::body::Body>,
) -> Result<
    (axum::http::StatusCode, axum::Json<HttpRequest>),
    (axum::http::StatusCode, axum::Json<String>),
> {
    let struct_fs_http_request = appiware_sdk::axum::from_axum_request(request).await;
    debug!("Handling request: {:?}", struct_fs_http_request);
    Ok((
        axum::http::StatusCode::OK,
        axum::Json(struct_fs_http_request),
    ))
}

static LOGGER: host::log::Logger = host::log::Logger::new(log::Level::Trace);

host::appiware_main! {|| {
    let _ = log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(log::LevelFilter::Trace))
        .map_err(|error| anyhow::anyhow!("{}", error));

    info!("Running EchoServer...");
    executor::run(
        // TODO(alex): Catch top level error on execution.
        async move { appiware_sdk::axum::wrap_for_server_protocol(build_router()).await },
    );

    None
}}
