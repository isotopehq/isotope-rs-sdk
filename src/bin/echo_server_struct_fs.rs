#![no_main]
use std::collections::BTreeMap;

use log::{debug, info};
use serde_json::value::Value as JsonValue;

use appiware_sdk::host;
use appiware_sdk::struct_fs;

use struct_fs::protocols::server::*;
use struct_fs::{path, Path, Reader, Writer};

const OUTSTANDING_REQUESTS_PREFIX: &str = "outstanding";

type RequestId = u64;

pub struct EchoServer<'host> {
    host: &'host mut host::Store,
    responses: BTreeMap<RequestId, Request>,
    next_request_id: RequestId,
}

impl<'host> EchoServer<'host> {
    pub fn new(host: &'host mut host::Store) -> anyhow::Result<Self> {
        Ok(EchoServer {
            host,
            responses: BTreeMap::new(),
            next_request_id: 0,
        })
    }

    pub fn run(&mut self) -> anyhow::Result<()> {
        info!("Running EchoServer...");
        let requests_path = path!(SERVER_REQUESTS_PATH);
        let outstanding_requests_path: Path =
            path!(SERVER_REQUESTS_PATH).join(&path!(OUTSTANDING_REQUESTS_PREFIX));
        while let Some(request) = self.host.read_owned::<Request>(&requests_path)? {
            debug!("Handling request: {:?}", request);
            match request.kind {
                RequestType::Read => {
                    // TODO(alex): Make it so that request.path is just the path to the server, not
                    // the full path visible _within_ the server.  I.e. a request read from
                    // 'ctx/server/requests/foo' should have a path 'foo' not
                    // 'ctx/server/requests/foo'.
                    let request_id_path = remove_prefix(&outstanding_requests_path, &request.path)?;
                    if request_id_path.len() != 1 {
                        return Err(anyhow::anyhow!(
                            "Request path ended in non-request_id component: {request_id_path:}"
                        ));
                    }

                    if let Some(payload) = self.responses.remove(&request_id_path[0].parse()?) {
                        self.host.write(
                            &request.request,
                            Response::<&Option<JsonValue>>::Read(Ok(Some(&payload.data))),
                        )?;
                    } else {
                        self.host.write(
                            &request.request,
                            Response::<&Option<JsonValue>>::Read(Err(
                                "Request was unfulfillable: no matching request found.".to_string(),
                            )),
                        )?;
                    }
                }
                RequestType::Write => {
                    let request_id = self.next_request_id;
                    self.next_request_id += 1;

                    let response_path = request.request.clone();

                    self.responses.insert(request_id, request);

                    let result = outstanding_requests_path.join(&path!(&format!("{request_id}")));
                    self.host
                        .write(&response_path, Response::<()>::Write(Ok(result.clone())))?;
                    debug!(
                        "Request fulfilled with `write/{} {}`",
                        response_path, result
                    );
                }
            }
        }
        info!("Synchronous request read returned None");

        info!("EchoServer shutting down...");
        Ok(())
    }
}

fn remove_prefix(prefix: &Path, p: &Path) -> anyhow::Result<Path> {
    if p.has_prefix(prefix) {
        Ok(p.slice_as_path(prefix.len(), p.len()))
    } else {
        Err(anyhow::anyhow!(
            "Path '{p:}' did not have prefix '{prefix:}'"
        ))
    }
}

static LOGGER: host::log::Logger = host::log::Logger::new(log::Level::Trace);

fn main_impl(host: &mut host::Store) -> anyhow::Result<()> {
    log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(log::LevelFilter::Trace))
        .map_err(|error| anyhow::anyhow!("{}", error))?;

    let mut server = EchoServer::new(host)?;
    server.run()
}

host::appiware_main! {|| {
    appiware_sdk::block::run(main_impl);

    None
}}
