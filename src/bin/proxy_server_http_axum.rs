#![no_main]
use lazy_static::lazy_static;

use appiware_sdk::host;
use appiware_sdk::struct_fs;

use struct_fs::{path, Path};

pub fn error_to_500(error: struct_fs::StoreError) -> (axum::http::StatusCode, axum::Json<String>) {
    (
        axum::http::StatusCode::INTERNAL_SERVER_ERROR,
        axum::Json(format!("Internal server error: {}", error)),
    )
}

fn extract_proxy_uri(uri: &axum::http::Uri) -> anyhow::Result<String> {
    log::debug!("Extracting proxy URI from: {uri}");
    let path_and_query = uri.path_and_query().ok_or_else(|| {
        anyhow::anyhow!(concat!(
            "Could not extract proxy target URI: source URI had no path.  ",
            "Path should represent the HTTPS target of the proxy request."
        ))
    })?;

    let path = path_and_query.path();
    if path.is_empty() || path == "/" {
        return Err(anyhow::anyhow!(concat!(
            "Could not extract proxy target URI: source URI had no path.  ",
            "Path should represent the HTTPS target of the proxy request."
        )));
    }

    log::debug!("URI base extracted: {path}");
    if let Some(query) = path_and_query.query() {
        Ok(format!("https://{path}?{query}"))
    } else {
        Ok(format!("https://{path}"))
    }
}

fn build_router() -> axum::Router {
    axum::Router::new()
        .route("/", axum::routing::any(proxy))
        .route("/*path", axum::routing::any(proxy))
}

async fn proxy(
    mut req: axum::http::request::Request<axum::body::Body>,
) -> Result<axum::http::Response<axum::body::Body>, (axum::http::StatusCode, axum::Json<String>)> {
    lazy_static! {
        static ref HTTP_STORE_PATH: Path = path!(struct_fs::protocols::http::OUTBOUND_HTTP_PATH);
    }

    log::debug!("Extracting proxy URI from request path and query.");

    let target_uri = extract_proxy_uri(req.uri()).map_err(|error| {
        (
            axum::http::StatusCode::INTERNAL_SERVER_ERROR,
            axum::Json(format!("Internal server error: {error}")),
        )
    })?;
    log::debug!("Proxying to {target_uri}");
    // Err((
    //     axum::http::StatusCode::IM_A_TEAPOT,
    //     axum::Json(format!("Test")),
    // ))

    let body_bytes = match axum::body::HttpBody::data(req.body_mut()).await {
        None => vec![],
        Some(body_bytes) => match body_bytes {
            Ok(bytes) => bytes.to_vec(),
            Err(_) => vec![],
        },
    };
    let outbound_request: struct_fs::protocols::http::Request =
        struct_fs::protocols::http::Request {
            uri: target_uri,
            method: req.method().to_string(),
            headers: req.headers().clone(),
            body: body_bytes,
        };

    let request_handle =
        host::store::write(&HTTP_STORE_PATH, outbound_request).map_err(error_to_500)?;
    let response = host::store::read::<struct_fs::protocols::http::Response>(&request_handle)
        .map_err(error_to_500)?;

    if let Some(res) = response {
        let mut response_builder = axum::http::Response::builder();
        // TODO(alex): Is there a way not to have to copy this HeaderMap struct?
        for h in res.headers.iter() {
            response_builder = response_builder.header(h.0, h.1);
        }
        Ok(response_builder
            .body(axum::body::Body::from(res.body))
            .unwrap())
    } else {
        Err((
            axum::http::StatusCode::INTERNAL_SERVER_ERROR,
            axum::Json("Internal server error".to_string()),
        ))
    }
}

static LOGGER: host::log::Logger = host::log::Logger::new(log::Level::Trace);

host::appiware_main! {|| {
    let _ = log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(log::LevelFilter::Trace))
        .map_err(|error| anyhow::anyhow!("{}", error));

    executor::run(
        // TODO(alex): Catch top level error on execution.
        async move { appiware_sdk::axum::wrap_for_server_protocol(build_router()).await },
    );

    None
}}
