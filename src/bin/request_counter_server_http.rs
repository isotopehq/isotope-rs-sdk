#![no_main]
use log::{debug, info};
use serde_json::value::Value as JsonValue;

use appiware_sdk::host;
use appiware_sdk::struct_fs;

use struct_fs::protocols::http::Response as HttpResponse;
use struct_fs::protocols::server::*;
use struct_fs::{path, Path, Reader, Writer};

pub struct CounterServer<'host> {
    host: &'host mut host::Store,
}

impl<'host> CounterServer<'host> {
    pub fn new(host: &'host mut host::Store) -> anyhow::Result<Self> {
        Ok(CounterServer { host })
    }

    pub fn run(&mut self) -> anyhow::Result<()> {
        info!("Running CounterServer...");
        let requests_path = path!(SERVER_REQUESTS_PATH);
        let persistent_store_path: Path =
            path!(struct_fs::protocols::persist::PERSISTENT_BLOCK_STORE_PATH);
        let counter_path = persistent_store_path.join(&path!("requests/counter"));

        while let Some(request) = self.host.read_owned::<Request>(&requests_path)? {
            debug!("Handling request: {:?}", request);
            match request.kind {
                RequestType::Read => {
                    let request_count = self.host.read_owned::<usize>(&counter_path)?.unwrap_or(0);
                    let result_body: JsonValue = serde_json::json!(request_count);

                    let mut headers = http::header::HeaderMap::new();
                    headers.append(
                        http::header::CONTENT_TYPE,
                        http::HeaderValue::from_static("application/json"),
                    );
                    let response = HttpResponse {
                        status: 200,
                        headers,
                        body: serde_json::to_string(&result_body).unwrap().into_bytes(),
                    };

                    self.host.write(
                        &request.request,
                        Response::<&HttpResponse>::Read(Ok(Some(&response))),
                    )?;
                    debug!(
                        "Request fulfilled with write/{} {response:?}",
                        request.request
                    );
                }
                RequestType::Write => {
                    let request_count = match self.host.read_owned::<usize>(&counter_path) {
                        Ok(Some(count)) => {
                            self.host.write(&counter_path, count + 1)?;
                            count
                        }
                        Ok(None) => {
                            // If this path doesn't exist, we haven't initialized our store yet, so
                            // let's do that.
                            let count = 0;
                            self.host.write(
                                &persistent_store_path,
                                serde_json::json!({
                                    "requests": {"counter": count}
                                }),
                            )?;
                            count
                        }
                        Err(error) => (Err(error))?,
                    };

                    // All requests are independent, so just send the caller back here to read the
                    // next response.  A more "accurate" implementation might try and match the
                    // appropriate count of requests to the requestor... e.g. make sure that
                    // requests in order A, B, C always receive n+1, n+2, n+3 respectively instead
                    // of in whatever order they _happen_ to be fulfilled.
                    self.host
                        .write(&request.request, Response::<()>::Write(Ok(request.path)))?;
                    debug!(
                        "Request fulfilled with `write/{} {}`",
                        &request.request, request_count
                    );
                }
            }
        }
        info!("Synchronous request read returned None");

        info!("CounterServer shutting down...");
        Ok(())
    }
}

static LOGGER: host::log::Logger = host::log::Logger::new(log::Level::Trace);

fn main_impl(host: &mut host::Store) -> anyhow::Result<()> {
    log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(log::LevelFilter::Trace))
        .map_err(|error| anyhow::anyhow!("{}", error))?;

    let mut server = CounterServer::new(host)?;
    server.run()
}

host::appiware_main! {|| {
    appiware_sdk::block::run(main_impl);

    None
}}
