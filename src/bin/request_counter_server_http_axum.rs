#![no_main]
use lazy_static::lazy_static;

use appiware_sdk::host;
use appiware_sdk::struct_fs;

use struct_fs::{path, Path};

pub fn error_to_500(error: struct_fs::StoreError) -> (axum::http::StatusCode, axum::Json<String>) {
    (
        axum::http::StatusCode::INTERNAL_SERVER_ERROR,
        axum::Json(format!("Internal server error: {}", error)),
    )
}

fn build_router() -> axum::Router {
    axum::Router::new()
        .route("/", axum::routing::get(increment))
        .route("/*path", axum::routing::get(increment))
}

async fn increment(
) -> Result<(axum::http::StatusCode, axum::Json<usize>), (axum::http::StatusCode, axum::Json<String>)>
{
    lazy_static! {
        static ref PERSISTENT_STORE_PATH: Path =
            path!(struct_fs::protocols::persist::PERSISTENT_BLOCK_STORE_PATH);
        static ref COUNTER_PATH: Path = PERSISTENT_STORE_PATH.join(&path!("requests/counter"));
    }

    let request_count = match host::store::read::<usize>(&COUNTER_PATH).map_err(error_to_500)? {
        Some(count) => {
            host::store::write(&COUNTER_PATH, count + 1).map_err(error_to_500)?;
            count
        }
        None => {
            // If this path doesn't exist, we haven't initialized our store yet, so
            // let's do that.
            let count = 0;
            host::store::write(
                &PERSISTENT_STORE_PATH,
                serde_json::json!({
                    "requests": {"counter": count}
                }),
            )
            .map_err(error_to_500)?;
            count
        }
    };

    Ok((axum::http::StatusCode::OK, axum::Json(request_count)))
}

static LOGGER: host::log::Logger = host::log::Logger::new(log::Level::Trace);

host::appiware_main! {|| {
    let _ = log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(log::LevelFilter::Trace))
        .map_err(|error| anyhow::anyhow!("{}", error));

    executor::run(
        // TODO(alex): Catch top level error on execution.
        async move { appiware_sdk::axum::wrap_for_server_protocol(build_router()).await },
    );

    None
}}
