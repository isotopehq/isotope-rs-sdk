#![no_main]
use log::info;

use appiware_sdk::host;
use appiware_sdk::struct_fs;

pub fn error_to_500(error: struct_fs::StoreError) -> (axum::http::StatusCode, axum::Json<String>) {
    (
        axum::http::StatusCode::INTERNAL_SERVER_ERROR,
        axum::Json(format!("Internal server error: {}", error)),
    )
}

fn build_router() -> axum::Router {
    axum::Router::new()
        .route("/", axum::routing::any(true_handler))
        .route("/*path", axum::routing::any(true_handler))
}

async fn true_handler(
) -> Result<(axum::http::StatusCode, axum::Json<bool>), (axum::http::StatusCode, axum::Json<String>)>
{
    Ok((axum::http::StatusCode::OK, axum::Json(true)))
}

static LOGGER: host::log::Logger = host::log::Logger::new(log::Level::Trace);

host::appiware_main! {|| {
    let _ = log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(log::LevelFilter::Trace))
        .map_err(|error| anyhow::anyhow!("{}", error));

    info!("Running TrueServer...");
    executor::run(
        // TODO(alex): Catch top level error on execution.
        async move { appiware_sdk::axum::wrap_for_server_protocol(build_router()).await },
    );

    None
}}
