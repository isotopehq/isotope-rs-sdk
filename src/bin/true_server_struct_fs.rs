#![no_main]
use log::{debug, info};

use appiware_sdk::host;
use appiware_sdk::struct_fs;

use struct_fs::protocols::server::*;
use struct_fs::{path, Path, Reader, Writer};

pub struct TrueServer<'host> {
    host: &'host mut host::Store,
}

impl<'host> TrueServer<'host> {
    pub fn new(host: &'host mut host::Store) -> anyhow::Result<Self> {
        Ok(TrueServer { host })
    }

    pub fn run(&mut self) -> anyhow::Result<()> {
        info!("Running TrueServer...");
        let requests_path = path!(SERVER_REQUESTS_PATH);
        while let Some(request) = self.host.read_owned::<Request>(&requests_path)? {
            debug!("Handling request: {:?}", request);
            match request.kind {
                RequestType::Read => {
                    self.host
                        .write(&request.request, Response::<bool>::Read(Ok(Some(true))))?;
                    debug!("Request fulfilled with `write/{} true`", request.request);
                }
                RequestType::Write => {
                    // Direct the client to read instead of write for a true value.
                    self.host.write(
                        &request.request,
                        Response::<()>::Write(Ok(requests_path.join(&path!("true")))),
                    )?;
                    debug!("Request fulfilled with `write/{} `", request.request);
                }
            }
        }
        info!("Synchronous request read returned None");

        info!("TrueServer shutting down...");
        Ok(())
    }
}

static LOGGER: host::log::Logger = host::log::Logger::new(log::Level::Trace);

fn main_impl(host: &mut host::Store) -> anyhow::Result<()> {
    log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(log::LevelFilter::Trace))
        .map_err(|error| anyhow::anyhow!("{}", error))?;

    let mut server = TrueServer::new(host)?;
    server.run()
}

host::appiware_main! {|| {
    appiware_sdk::block::run(main_impl);

    None
}}
