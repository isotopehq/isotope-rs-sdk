#[macro_export]
macro_rules! appiware_main {
    ($body:expr) => {
        #[no_mangle]
        pub fn sdk_host_start_impl() -> Option<String> {
            $body()
        }
    };
}

mod host_interface_start_impl {
    // Allow "lazy" implementation of start function while also forcing wit-bindgen codegen to
    // happen in this crate.  Previously we tried to run the following `export!` macro in the
    // target crate, but the path resolution tried to load `host-interface.wit` from there instead
    // of here.  As a result, we have to fully implement HostInterface _here_ and delegate linkage
    // of the /real/ implementation until later... which we do with this `extern "Rust"`.
    extern "Rust" {
        fn sdk_host_start_impl() -> Option<String>;
    }

    wit_bindgen_guest_rust::export!("src/host/host-interface.wit");

    struct HostInterface {}

    impl host_interface::HostInterface for HostInterface {
        fn start() -> Option<String> {
            unsafe { sdk_host_start_impl() }
        }
    }
}

pub use crate::appiware_main;
