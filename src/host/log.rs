use log::{Level, Metadata, Record};
use serde_json::json;

use super::client_interface;

#[derive(thiserror::Error, Debug)]
pub enum LoggerError {
    #[error("The logger encountered an unknown error during logging")]
    Unknown,
}

pub struct Logger {
    waterline: Level,
}

impl Logger {
    pub const fn new(waterline: Level) -> Self {
        Logger { waterline }
    }

    fn write(path: &str, value: &[u8]) -> Result<(), LoggerError> {
        let _path_string = client_interface::write(path, value).map_err(|_| LoggerError::Unknown);

        Ok(())
    }
}

fn log_logger_error(msg: &str) -> Result<(), LoggerError> {
    Logger::write(
        "log/error/console/rust_logger",
        json!({ "msg": msg }).to_string().as_bytes(),
    )
}

impl log::Log for Logger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= self.waterline
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let target = record
                .target()
                .split("::")
                .map(|token| {
                    if token.starts_with("_") {
                        format!("m{}", token)
                    } else {
                        token.to_string()
                    }
                })
                .collect::<Vec<String>>()
                .join("/");
            let level = record.level().as_str().to_lowercase();

            let log_path_string = format!("log/{}/console/{}", level, target);

            let message = serde_json::to_string(&json!({ "msg": format!("{}", record.args()) }))
                .map_err(|error| {
                    log_logger_error(&format!(
                        "Error occurred when serializing record to log: {}",
                        error
                    ))
                    .expect("An unknown error occurred when writing");

                    error
                })
                .unwrap();

            Logger::write(&log_path_string, message.as_bytes())
                .map_err(|error| {
                    log_logger_error(&format!(
                        "Error occurred when attempting to log message ({}) to path ({}): {}",
                        message, log_path_string, error
                    ))
                    .expect("An unknown error occurred when writing");

                    error
                })
                .unwrap();
        }
    }

    fn flush(&self) {}
}
