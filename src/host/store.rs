use serde::de::DeserializeOwned;
use serde::Serialize;

use crate::struct_fs;

use super::client_interface;
use struct_fs::{Path, Reader, Writer};

pub fn read_raw(from: &str) -> Result<Option<Vec<u8>>, struct_fs::StoreError> {
    client_interface::read(from).map_err(|error_string| struct_fs::StoreError::RawError {
        message: format!("Host read request failed: {}", error_string),
    })
}

fn read_to_deserializer<'de>(
    from: &Path,
) -> Result<Option<Box<dyn erased_serde::Deserializer<'de>>>, struct_fs::StoreError> {
    let value_bytes = read_raw(&from.to_string())?;

    if let Some(bytes) = value_bytes {
        // Having serde_json::Value isn't necessarily the most efficient here, but it provides a
        // concrete intermediate that we can forward along.
        let value: serde_json::Value =
            serde_json::from_slice(bytes.as_slice()).map_err(|error| {
                struct_fs::StoreError::RecordDeserializationError {
                    message: error.to_string(),
                }
            })?;

        Ok(Some(Box::new(
            <dyn erased_serde::Deserializer<'de>>::erase(value),
        )))
    } else {
        Ok(None)
    }
}

pub fn read<RecordType: DeserializeOwned>(
    from: &Path,
) -> Result<Option<RecordType>, struct_fs::StoreError> {
    if let Some(deserializer) = read_to_deserializer(from)? {
        return Ok(Some(RecordType::deserialize(deserializer).map_err(
            |error| struct_fs::StoreError::RecordDeserializationError {
                message: error.to_string(),
            },
        )?));
    }

    Ok(None)
}

#[derive(Default)]
pub struct Store {}

impl Reader for Store {
    fn read_to_deserializer<'de, 'this>(
        &'this mut self,
        from: &Path,
    ) -> Result<Option<Box<dyn erased_serde::Deserializer<'de>>>, struct_fs::StoreError>
    where
        'this: 'de,
    {
        read_to_deserializer(from)
    }

    fn read_owned<RecordType: DeserializeOwned>(
        &mut self,
        from: &Path,
    ) -> Result<Option<RecordType>, struct_fs::StoreError> {
        if let Some(deserializer) = self.read_to_deserializer(from)? {
            return Ok(Some(RecordType::deserialize(deserializer).map_err(
                |error| struct_fs::StoreError::RecordDeserializationError {
                    message: error.to_string(),
                },
            )?));
        }

        Ok(None)
    }
}

pub fn write_raw(to: &str, data: &[u8]) -> Result<String, struct_fs::StoreError> {
    client_interface::write(to, data).map_err(|error_string| struct_fs::StoreError::UnknownError {
        message: error_string,
    })
}

pub fn write<D: Serialize>(to: &Path, data: D) -> Result<Path, struct_fs::StoreError> {
    let data_json = serde_json::to_string(&data).map_err(|error| {
        struct_fs::StoreError::RecordSerializationError {
            message: error.to_string(),
        }
    })?;

    let path_string = write_raw(&to.to_string(), data_json.as_bytes())?;

    Path::parse(&path_string).map_err(|err| err.into())
}

impl Writer for Store {
    fn write<D: Serialize>(&mut self, to: &Path, data: D) -> Result<Path, struct_fs::StoreError> {
        write(to, data)
    }
}
