//! Appiware Rust SDK
//!
//! This SDK is intended for use in writing Appiware blocks.

pub mod host;
pub mod struct_fs;

#[cfg(feature = "axum")]
pub mod axum;

pub mod block {
    use super::*;
    use log::{error, info};

    /// Wraps a Block main function and routes errors to logging.
    fn log_root_errors(main_impl: fn(&mut host::Store) -> anyhow::Result<()>) {
        let mut host = host::Store::default();

        if let Err(error) = main_impl(&mut host) {
            info!("Block exiting with error...");
            error!("{}", &error.to_string());
        }
    }

    /// Recommended entrypoint for Appiware Block execution
    ///
    /// Errors returned by `main_impl` will be automatically logged.
    pub fn run(main_impl: fn(&mut host::Store) -> anyhow::Result<()>) {
        log_root_errors(main_impl);
    }
}
