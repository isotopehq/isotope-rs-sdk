mod path;
pub use path::{path, Error as PathError, Path};

mod store;
pub use store::{Error as StoreError, Reader, Writer};

pub mod protocols {
    use super::*;
    pub mod server {
        use super::*;

        use serde::{Deserialize, Serialize};
        use serde_json::value::Value as JsonValue;

        pub const SERVER_REQUESTS_PATH: &str = "ctx/server/requests";

        #[derive(Deserialize, Clone, Debug)]
        pub enum RequestType {
            Read,
            Write,
        }

        #[derive(Deserialize, Clone, Debug)]
        pub struct Request {
            pub request: Path,
            #[serde(rename = "type")]
            pub kind: RequestType,
            pub path: Path,
            pub data: Option<JsonValue>,
        }

        #[derive(Serialize, Clone, Debug)]
        pub enum Response<RecordType> {
            Read(Result<Option<RecordType>, String>),
            Write(Result<Path, String>),
        }
    }

    pub mod http {
        use serde::{Deserialize, Serialize};

        pub const OUTBOUND_HTTP_PATH: &str = "ctx/http";

        #[derive(Serialize, Deserialize, Clone, Debug)]
        pub struct Request {
            // TODO(alex): Rename s/uri/path/ as this isn't actually a URI
            pub uri: String,
            pub method: String,
            #[serde(with = "http_serde::header_map")]
            pub headers: http::header::HeaderMap,
            #[serde(with = "super::base64_helper")]
            pub body: Vec<u8>,
        }

        #[derive(Serialize, Deserialize, Clone, Debug)]
        pub struct Response {
            pub status: u16,
            #[serde(with = "http_serde::header_map")]
            pub headers: http::header::HeaderMap,
            #[serde(with = "super::base64_helper")]
            pub body: Vec<u8>,
        }
    }

    pub mod persist {
        pub const PERSISTENT_BLOCK_STORE_PATH: &str = "ctx/block/persist";
    }
}

pub mod base64_helper {
    /// Inspired by https://users.rust-lang.org/t/serialize-a-vec-u8-to-json-as-base64/57781/4
    use serde::{Deserializer, Serializer};

    pub fn serialize<S: Serializer>(v: &[u8], serializer: S) -> Result<S::Ok, S::Error> {
        serializer.collect_str(&base64::display::Base64Display::with_config(
            v,
            base64::STANDARD,
        ))
    }

    pub fn deserialize<'de, D: Deserializer<'de>>(deserializer: D) -> Result<Vec<u8>, D::Error> {
        struct Vis;
        impl serde::de::Visitor<'_> for Vis {
            type Value = Vec<u8>;

            fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
                formatter.write_str("a base64 string")
            }

            fn visit_str<E: serde::de::Error>(self, v: &str) -> Result<Self::Value, E> {
                base64::decode(v).map_err(serde::de::Error::custom)
            }
        }

        deserializer.deserialize_str(Vis)
    }
}
