use serde::de::DeserializeOwned;
use serde::Serialize;

use super::{Path, PathError};

#[derive(thiserror::Error, Debug, PartialEq, Eq)]
pub enum Error {
    #[error("{0}")]
    PathError(#[from] PathError),
    #[error("An error occurred while serializing a record: {message}")]
    RecordSerializationError { message: String },
    #[error("An error occurred while deserializing a record: {message}")]
    RecordDeserializationError { message: String },
    #[error("An implementation error occurred: {message}")]
    StoreImplementationError { message: String },
    #[error("Error: {message}")]
    RawError { message: String },
    #[error("An unknown error occurred: {message}")]
    UnknownError { message: String },
}

pub trait Writer {
    // TODO(alex): Make a record method like `doc()` in Firestore which allows direct record
    // management instead of having to juggle the Store implementation everywhere.

    /// Writes `data` to `destination` within the store.
    ///
    /// Note that the path written to must be compatible with the existing tree up
    /// until the last path component.  I.e. the parent of the new data must be a
    /// compatible structure where that path can be written as a child.
    ///
    /// Example: writing `"foo"` at path `bar/baz/qux` into
    /// ```json
    /// {
    ///     "bar": "baz"
    /// }
    /// ```
    /// will result in an error as the string `"baz"` is not a valid parent for
    /// the key/value pair `"qux": "foo"`.
    ///
    /// To achieve the likely desired result, either the write should be broken
    /// down to multiple steps building up the tree, or a "larger" structure
    /// should be written "higher up" in the record hierarchy: e.g. write
    /// `{"qux": "foo"}` at `bar/baz` instead (this works as the structure at
    /// `"bar"` supports key/value children).
    ///
    /// The returned `Path` represents the "result" of the written record.
    /// Different `Store` backends may have different meanings for a result
    /// `Path`.  A simple in-memory store might just return the same path as
    /// that provided, while a synthetic store embedding a client handling
    /// an HTTP POST request may return the `Path` to find the response.
    // TODO(alex): Switch to accepting `data` by reference as not all stores make use of owned
    // values.
    fn write<RecordType: Serialize>(
        &mut self,
        destination: &Path,
        data: RecordType,
    ) -> Result<Path, Error>;
}

impl<T: Writer> Writer for &mut T {
    fn write<RecordType: Serialize>(
        &mut self,
        destination: &Path,
        data: RecordType,
    ) -> Result<Path, Error> {
        (*self).write(destination, data)
    }
}

pub trait Reader {
    // TODO(alex): When GATs are supported, constrain this to implement `serde::Deserializer`.

    /// The type implementing a `serde::Deserializer` that will "unpack" the read value.
    ///
    /// By providing both serde::Deserializer and serde::Deserialize variants of read, the caller
    /// can decide how best to consume the deserializing type.
    ///
    /// This currently requires `erased_serde` in the public interface as the GAT solution
    /// proved...  difficult.
    fn read_to_deserializer<'de, 'this>(
        &'this mut self,
        from: &Path,
    ) -> Result<Option<Box<dyn erased_serde::Deserializer<'de>>>, Error>
    where
        'this: 'de;

    fn read_owned<RecordType: DeserializeOwned>(
        &mut self,
        from: &Path,
    ) -> Result<Option<RecordType>, Error>;
}

pub trait Store: Writer + Reader {}
impl<T> Store for T where T: Writer + Reader {}
